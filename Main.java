
//This program was written by Emre YIGIT in first semester at Marmara University for project of CSE1100 class.
/*Purpose of this program is converting from binary to what user want the type of
    unsigned int, 1s comp, 2s comp, ISA or char by given input file.
*/
import java.io.*;
import java.util.*;

public class Main {

    public static int getUnSigned(String binary) {	// to convert to unsigned integer from binary
        return Integer.parseInt(binary, 2);
    }

    public static int getOnesComplement(String binary) {	// to convert to 1's complement integer from binary
        if (binary.charAt(0) == '1') {
            String invertedInt = invertDigits(binary);
            int decimalValue = Integer.parseInt(invertedInt, 2);
            decimalValue = decimalValue * -1;
            return decimalValue;
        } else
            return Integer.parseInt(binary, 2);
    }

    public static int getTwosComplement(String binary) {	// to convert to 2's complement integer from binary
        if (binary.charAt(0) == '1') {
            String invertedInt = invertDigits(binary);
            int decimalValue = Integer.parseInt(invertedInt, 2);
            decimalValue = (decimalValue + 1) * -1;
            return decimalValue;
        } else
            return Integer.parseInt(binary, 2);
    }

    public static char getAsciiCharacter(String binary) {	// to convert to Ascii character from binary
        int decimalValue = Integer.parseInt(binary, 2);
        char asciiChar = (char) decimalValue;
        return asciiChar;
    }

    public static String getLc3Instruction(String binary) {		// to convert to LC-3 Instructions from binary
        String instruction = "";
        if (binary.substring(0, 4).equals("0001")) {
            instruction += "ADD ";
            int dr = Integer.parseInt(binary.substring(4, 7), 2);
            int sr1 = Integer.parseInt(binary.substring(7, 10), 2);
            instruction += ("R" + dr + ", R" + sr1 + ", ");
            if (binary.charAt(10) == '0') {
                int sr2 = Integer.parseInt(binary.substring(13), 2);
                instruction += ("R" + sr2);
            } else {
                int imm5 = getTwosComplement(binary.substring(11));
                instruction += ("#" + imm5);
            }
        } else if (binary.substring(0, 4).equals("0101")) {
            instruction += "AND ";
            int dr = Integer.parseInt(binary.substring(4, 7), 2);
            int sr1 = Integer.parseInt(binary.substring(7, 10), 2);
            instruction += ("R" + dr + ", R" + sr1 + ", ");
            if (binary.charAt(10) == '0') {
                int sr2 = Integer.parseInt(binary.substring(13), 2);
                instruction += ("R" + sr2);
            } else {
                int imm5 = getTwosComplement(binary.substring(11));
                instruction += ("# " + imm5);
            }
        } else if (binary.substring(0, 4).equals("1110")) {
            instruction += "LEA ";
            int dr = Integer.parseInt(binary.substring(4, 7), 2);
            int PcOffset11 = getTwosComplement(binary.substring(7));
            instruction += ("R" + dr + ", #" + PcOffset11);
        } else if (binary.substring(0, 4).equals("1001")) {
            instruction += "NOT ";
            int dr = Integer.parseInt(binary.substring(4, 7), 2);
            int sr = Integer.parseInt(binary.substring(7, 10), 2);
            instruction += ("R" + dr + ", R" + sr);
        }
        return instruction;
    }

    public static String invertDigits(String binary) {		// to flip ones to zeros and zeros to ones
        String result = binary;
        result = result.replace("0", " ");
        result = result.replace("1", "0");
        result = result.replace(" ", "1");
        return result;
    }

    public static void main(String[] args) {
        // TODO Auto-generated method stub

        /* //I got help from this site for reading and writing files:
         * "https://gelecegiyazanlar.turkcell.com.tr/konu/android/egitim/android-101/javada-dosya-islemleri"
         */
        String str = "";
        ArrayList<String> input = new ArrayList<String>();
        try {
            FileInputStream fStream = new FileInputStream("in.txt");
            DataInputStream dStream = new DataInputStream(fStream);
            BufferedReader bReader = new BufferedReader(new InputStreamReader(dStream));

            while ((str = bReader.readLine()) != null) {	// this loop adds it to the array list as a line element that reads from the in.txt
                input.add(str);
            }
            bReader.close();
            dStream.close();
            fStream.close();
        } catch (Exception e) {
            System.out.println("Hata : " + e.getMessage());
        }
        ArrayList<String> output = new ArrayList<String>();
        for (int i = 0; i < input.size(); i++) {			//	this loop splits each lines into two pieces
            String[] dataArray = input.get(i).split("	");	//	each line should contain a tab space to be used as separator
            for (int j = 0; j < dataArray.length; j++) {

                if (j == 1 && dataArray[j].equals("S2")) {		// these if functions are used to determine which method to use
                    output.add("" + getTwosComplement(dataArray[0]));
                } else if (j == 1 && dataArray[j].equals("S1")) {
                    output.add("" + getOnesComplement(dataArray[0]));
                } else if (j == 1 && dataArray[j].equals("UN")) {
                    output.add("" + getUnSigned(dataArray[0]));
                } else if (j == 1 && dataArray[j].equals("CH")) {
                    output.add("" + getAsciiCharacter(dataArray[0]));
                } else if (j == 1 && dataArray[j].equals("INS")) {
                    output.add("" + getLc3Instruction(dataArray[0]));
                }
            }

        }
        FileWriter fWriter = null;
        BufferedWriter writer = null;
        try {
            File out = new File("out.txt.");
            fWriter = new FileWriter(out);
            writer = new BufferedWriter(fWriter);
            for (int i = 0; i < output.size(); i++) {
                writer.write(output.get(i));
                writer.newLine();
            }
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
